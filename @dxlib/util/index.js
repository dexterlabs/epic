export function aria(props) {
  return Object.keys(props)
    .reduce((acc, prop) => ({
      ...acc,
      [`aria-${prop}`]: props[prop]
    }), {})
}